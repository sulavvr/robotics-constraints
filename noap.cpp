/**
 * Name: Sulav Regmi
 * Robotics Software Control
 * Calculating n, o, a, p using equations in Chapter 3.
 * Calculating control variables theta1, theta2, theta3, theta4 and theta5 after.
 *
 * Dr. Hammerand
 */
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

const double PI = atan(1) * 4;
// sine and cosine angle calculating functions
double sine(double);
double cosine(double);
double tanInverse(double);

int main ()
{
    double theta1, theta2, theta3, theta4, theta5;
    double a2, a3, d5;

    double n, o, a, p;
    double nx, ny, nz;
    double ox, oy, oz;
    double ax, ay, az;
    double px, py, pz;

    // Using values in Chapter 2 and 3
    a2 = a3 = d5 = 100;

    theta1 = -115;
    theta2 = 25;
    theta3 = 50;
    theta4 = 65;
    theta5 = -35;

    nx = ((cosine(theta1) * cosine(theta2) * cosine(theta3) - sine(theta1) * sine(theta3)) * cosine(theta4)
            - cosine(theta1) * sine(theta2) * sine(theta4)) * cosine(theta5)
            + (cosine(theta1) * cosine(theta2) * sine(theta3) + sine(theta1) * cosine(theta3)) * sine(theta5);

    ny = ((sine(theta1) * cosine(theta2) * cosine(theta3) + cosine(theta1) * sine(theta3)) * cosine(theta4)
            - sine(theta1) * sine(theta2) * sine(theta4)) * cosine(theta5)
            + (sine(theta1) * cosine(theta2) * sine(theta3) - cosine(theta1) * cosine(theta3)) * sine(theta5);

    nz = (sine(theta2) * cosine(theta3) * cosine(theta4) + cosine(theta2) * sine(theta4)) * cosine(theta5)
            + sine(theta2) * sine(theta3) * sine(theta5);

    ox = -((cosine(theta1) * cosine(theta2) * cosine(theta3) - sine(theta1) * sine(theta3)) * cosine(theta4)
            - cosine(theta1) * sine(theta2) * sine(theta4)) * sine(theta5)
            + (cosine(theta1) * cosine(theta2) * sine(theta3) + sine(theta1) * cosine(theta3)) * cosine(theta5);

    oy = -((sine(theta1) * cosine(theta2) * cosine(theta3) + cosine(theta1) * sine(theta3)) * cosine(theta4)
            - sine(theta1) * sine(theta2) * sine(theta4)) * sine(theta5)
            + (sine(theta1) * cosine(theta2) * sine(theta3) - cosine(theta1) * cosine(theta3)) * cosine(theta5);

    oz = -(sine(theta2) * cosine(theta3) * cosine(theta4) + cosine(theta2) * sine(theta4)) * sine(theta5)
            + sine(theta2) * sine(theta3) * cosine(theta5);

    ax = (cosine(theta1) * cosine(theta2) * cosine(theta3) - sine(theta1) * sine(theta3)) * sine(theta4)
            + cosine(theta1) * sine(theta2) * cosine(theta4);

    ay = (sine(theta1) * cosine(theta2) * cosine(theta3) + cosine(theta1) * sine(theta3)) * sine(theta4)
            + sine(theta1) * sine(theta2) * cosine(theta4);

    az = sine(theta2) * cosine(theta3) * sine(theta4) - cosine(theta2) * cosine(theta4);

    px = d5 * ((cosine(theta1) * cosine(theta2) * cosine(theta3) - sine(theta1) * sine(theta3)) * sine(theta4)
            + cosine(theta1) * sine(theta2) * cosine(theta4))
            + a3 * (cosine(theta1) * cosine(theta2) * cosine(theta3) - sine(theta1) * sine(theta3))
            + a2 * cosine(theta1) * cosine(theta2);

    py =  d5 * ((sine(theta1) * cosine(theta2) * cosine(theta3) + cosine(theta1) * sine(theta3)) * sine(theta4)
            + sine(theta1) * sine(theta2) * cosine(theta4))
            + a3 * (sine(theta1) * cosine(theta2) * cosine(theta3) + cosine(theta1) * sine(theta3))
            + a2 * sine(theta1) * cosine(theta2);

    pz = d5 * (sine(theta2) * cosine(theta3) * sine(theta4) - cosine(theta2) * cosine(theta4))
            + (a3 * cosine(theta3) + a2) * sine(theta2);

    cout << "Starting values for:" << endl;
    cout << "theta3: " << theta3 << endl;
    cout << "theta1: " << theta1 << endl;
    cout << "theta2: " << theta2 << endl;
    cout << "theta4: " << theta4 << endl;
    cout << "theta5: " << theta5 << endl;

    cout << endl << "-----------------------------------------------------------------------------" << endl;
    cout << "-----------------------------------------------------------------------------" << endl << endl;
    cout << "Ai matrix:" << endl;
    cout << "|nx  ox  ax  px|   | " << fixed << setprecision(3) << nx << "  " << setw(6) << ox << "  " << setw(6) << ax << "  " << setw(6) << px << "\t|" << endl;
    cout << "|ny  oy  ay  py| = | " << fixed << setprecision(3) << ny << "  " << setw(6) << oy << "  " << setw(6) << ay << "  " << setw(6) << py << "\t|" << endl;
    cout << "|nz  oz  az  pz|   | " << fixed << setprecision(3) << nz << "  " << setw(6) << oz << "  " << setw(6) << az << "  " << setw(6) << pz << "\t|" << endl;
    cout << "|0   0   0   1 |   | " << "0" << setw(8) << "0" << setw(8) << "0" << setw(12) << "1\t\t|" << endl;
    // cout << endl << "-----------------------------------------------------------------------------" << endl << endl;
    cout << endl << endl;

    double n_mag = sqrt(pow(nx, 2) + pow(ny, 2) + pow(nz, 2));
    double o_mag = sqrt(pow(ox, 2) + pow(oy, 2) + pow(oz, 2));
    double a_mag = sqrt(pow(ax, 2) + pow(ay, 2) + pow(az, 2));
    double p_mag = sqrt(pow(px, 2) + pow(py, 2) + pow(pz, 2));

    cout << "Magnitudes of n, o, a & p:" << endl;
    cout << "|n| = " << n_mag << endl;
    cout << "|o| = " << o_mag << endl;
    cout << "|a| = " << a_mag << endl;
    cout << "|p| = " << p_mag << endl;

    double n_cross_o[3], o_cross_a[3], a_cross_n[3];

    n_cross_o[0] = ny * oz - oy * nz;
    n_cross_o[1] = nz * ox - nx * oz;
    n_cross_o[2] = nx * oy - ny * ox;

    o_cross_a[0] = oy * az - ay * oz;
    o_cross_a[1] = oz * ax - ox * az;
    o_cross_a[2] = ox * ay - oy * ax;

    a_cross_n[0] = ay * nz - ny * az;
    a_cross_n[1] = az * nx - ax * nz;
    a_cross_n[2] = ax * ny - ay * nx;

    // cout << endl << "-----------------------------------------------------------------------------" << endl << endl;
    cout << endl << endl;
    cout << "        " << "| " << fixed << setprecision(3) << setw(7) << n_cross_o[0] << " |" << endl;
    cout << "n x o = " << "| " << fixed << setprecision(3) << setw(7) << n_cross_o[1] << " | = a" << endl;
    cout << "        " << "| " << fixed << setprecision(3) << setw(7) << n_cross_o[2] << " |" << endl;

    // cout << endl << "-----------------------------------------------------------------------------" << endl << endl;
    cout << endl << endl;
    cout << "        " << "| " << fixed << setprecision(3) << setw(7) << o_cross_a[0] << " |" << endl;
    cout << "o x a = " << "| " << fixed << setprecision(3) << setw(7) << o_cross_a[1] << " | = n" << endl;
    cout << "        " << "| " << fixed << setprecision(3) << setw(7) << o_cross_a[2] << " |" << endl;

    // cout << endl << "-----------------------------------------------------------------------------" << endl << endl;
    cout << endl << endl;
    cout << "        " << "| " << fixed << setprecision(3) << setw(7) << a_cross_n[0] << " |" << endl;
    cout << "a x n = " << "| " << fixed << setprecision(3) << setw(7) << a_cross_n[1] << " | = o" << endl;
    cout << "        " << "| " << fixed << setprecision(3) << setw(7) << a_cross_n[2] << " |" << endl;

    double n_cross_o_minus_a[3];
    n_cross_o_minus_a[0] = n_cross_o[0] - ax;
    n_cross_o_minus_a[1] = n_cross_o[1] - ay;
    n_cross_o_minus_a[2] = n_cross_o[2] - az;


    // cout << endl << "-----------------------------------------------------------------------------" << endl << endl;
    cout << endl << endl;
    cout << "|(n x o) - a| = " << sqrt(pow(n_cross_o_minus_a[0], 2) + pow(n_cross_o_minus_a[1], 2) + pow(n_cross_o_minus_a[2], 2)) << " < e";
    cout << endl;

    cout << endl << "-----------------------------------------------------------------------------" << endl;
    cout << "-----------------------------------------------------------------------------" << endl << endl;

    // From equations 3.44, 3.45 and 3.46
    double pwx = d5 * ax;
    double pwy = d5 * ay;
    double pwz = d5 * az;

    // From equations 3.47, 3.48 and 3.49
    double pax = px - pwx;
    double pay = py - pwy;
    double paz = pz - pwz;

    double _theta1, _theta2, _theta3, _theta4, _theta5;
    double sin_theta2, sin_theta3, sin_theta4, sin_theta5;
    double cos_theta2, cos_theta3, cos_theta4, cos_theta5;

    cos_theta3 = (pow(pax, 2) + pow(pay, 2) + pow(paz, 2) - pow(a3, 2) - pow(a2, 2)) / (2 * a2 * a3);
    sin_theta3 = sqrt(1 - pow(cos_theta3, 2));
    _theta3 = tanInverse(sin_theta3 / cos_theta3);

    cout << "Calculated values for:" << endl;
    cout << "theta3: " << setprecision(0) << _theta3 << endl;

    double sin_beta_minus_theta1 = (a3 * sin_theta3) / sqrt(pow(pax, 2) + pow(pay, 2));
    double cos_beta_minus_theta1 = sqrt(1 - pow(sin_beta_minus_theta1, 2));

    double beta_minus_theta1 = tanInverse(sin_beta_minus_theta1 / cos_beta_minus_theta1);
    double beta = tanInverse(pay / pax);

    _theta1 = beta - beta_minus_theta1;

    cout << "theta1: " << setprecision(0) << _theta1 << endl;

    cos_theta2 = (pax * cosine(theta1) + pay * sine(theta1)) / (a3 * cosine(theta3) + a2);
    sin_theta2 = paz / (a3 * cosine(theta3) + a2);
    _theta2 = tanInverse(sin_theta2 / cos_theta2);

    cout << "theta2: " << setprecision(0) << _theta2 << endl;

    cos_theta4 = ax * cosine(theta1) * sine(theta2) + ay * sine(theta1) * sine(theta2) - az * cosine(theta2);

    sin_theta4 = ax * (cosine(theta1) * cosine(theta2) * cosine(theta3) - sine(theta1) * sine(theta3))
                    + ay * (sine(theta1) * cosine(theta2) * cosine(theta3) + cosine(theta1) * sine(theta3))
                    + az * sine(theta2) * cosine(theta3);

    _theta4 = tanInverse(sin_theta4 / cos_theta4);

    cout << "theta4: " << setprecision(0) << _theta4 << endl;
    //c5 = ox(c1c2s3 + s1c3) + oy(s1c2s3 − c1c3) + ozs2s3
    //s5 = nx(c1c2s3 + s1c3) + ny(s1c2s3 − c1c3) + nzs2s3
    cos_theta5 = ox * (cosine(theta1) * cosine(theta2) * sine(theta3) + sine(theta1) * cosine(theta3))
                    + oy * (sine(theta1) * cosine(theta2) * sine(theta3) - cosine(theta1) * cosine(theta3))
                    + oz * sine(theta2) * sine(theta3);

    sin_theta5 = nx * (cosine(theta1) * cosine(theta2) * sine(theta3) + sine(theta1) * cosine(theta3))
                    + ny * (sine(theta1) * cosine(theta2) * sine(theta3) - cosine(theta1) * cosine(theta3))
                    + nz * sine(theta2) * sine(theta3);

    _theta5 = tanInverse(sin_theta5 / cos_theta5);

    cout << "theta5: " << setprecision(0) << _theta5 << endl;

    return 0;
}

/**
 * Calculate the sine angle
 * @param  angle
 * @return degree
 */
double sine(double angle)
{
    return sin(angle * PI / 180);
}

/**
 * Calculate the cosine angle
 * @param  angle
 * @return degree
 */
double cosine(double angle)
{
    return cos(angle * PI / 180);
}

double tanInverse(double angle)
{
    return atan(angle) * 180 / PI;
}
