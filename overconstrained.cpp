/**
 * Name: Sulav Regmi
 * Robotics Software Control
 * Over-constrained - 2 unknowns, 3 equations
 * x + 3y = -2
 * 2x - y = 1
 * 3x + 4y = -5
 *
 * Dr. Hammerand
 */

#include <iostream>
#include <cmath>
using namespace std;

 int main()
 {
    double x[3], y[3], c[3];
    cout << "1st Line: Enter x, y intercepts and a constant C of an equation:";
    cin >> x[0] >> y[0] >> c[0];
    cout << "Your equation is: "
            << (x[0] < 0 ? " -" : "") << fabs(x[0]) << "x"
            << (y[0] < 0 ? " - " : " + ") << fabs(y[0]) << "y ="
            << (c[0] < 0 ? " -" : " ") << fabs(c[0]) << endl;

    cout << "2nd Line: Enter x, y intercepts and a constant C of another equation:";
    cin >> x[1] >> y[1] >> c[1];
    cout << "Your equation is: "
            << (x[1] < 0 ? " -" : "") << fabs(x[1]) << "x"
            << (y[1] < 0 ? " - " : " + ") << fabs(y[1]) << "y = "
            << (c[1] < 0 ? " -" : " ") << fabs(c[1]) << endl;

    cout << "3rd Line: Enter x, y intercepts and a constant C of another equation:";
    cin >> x[2] >> y[2] >> c[2];
    cout << "Your equation is: "
            << (x[2] < 0 ? " -" : "") << fabs(x[2]) << "x"
            << (y[2] < 0 ? " - " : " + ") << fabs(y[2]) << "y = "
            << (c[2] < 0 ? " -" : " ") << fabs(c[2]) << endl;

    return 0;
 }
