/**
 * Name: Sulav Regmi
 * Robotics Software Control
 * Under-constrained - 3 unknowns, 2 equations
 * 3x + 2y - z = 5
 * x - 4y + 2z = 1
 *
 * Dr. Hammerand
 */

#include <iostream>
#include <cmath>
using namespace std;

 int main()
 {
    double x[2], y[2], z[2], c[2];
    cout << "1st Plane: Enter x, y, z intercepts and a constant C of an equation:";
    cin >> x[0] >> y[0] >> z[0] >> c[0];
    cout << "Your equation is: "
            << (x[0] < 0 ? " -" : "") << fabs(x[0]) << "x"
            << (y[0] < 0 ? " - " : " + ") << fabs(y[0]) << "y"
            << (z[0] < 0 ? " - " : " + ") << fabs(z[0]) << "z ="
            << (c[0] < 0 ? " -" : " ") << fabs(c[0]) << endl;

    cout << "2nd Plane: Enter x, y, z intercepts and a constant C of another equation:";
    cin >> x[1] >> y[1] >> z[1] >> c[1];
    cout << "Your equation is: "
            << (x[1] < 0 ? " -" : "") << fabs(x[1]) << "x"
            << (y[1] < 0 ? " - " : " + ") << fabs(y[1]) << "y"
            << (z[1] < 0 ? " - " : " + ") << fabs(z[1]) << "z ="
            << (c[1] < 0 ? " -" : " ") << fabs(c[1]) << endl;


    return 0;
 }
